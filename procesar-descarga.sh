#!/bin/bash

IFS=$'\n'

for archivos in archivos_descomprimidos/*;do


	if [ -d $archivos ];then

		for archivo in $archivos/*;do


			tipo_archivo=$(file -b "$archivo")


			if [[ $tipo_archivo == *"image"* ]]; then




				echo $(echo $archivo | cut -d "/" -f 3 ) > primer_nombre.txt 



				if [[ $(grep -Ew "^[[:upper:]][[:lower:]]+" primer_nombre.txt) == "" ]];then
					echo "la imagen no tiene un nombre adecuado"
					continue
				fi


				#reescala la imagen a 512x512 y la centra

				convert $archivo -gravity center -resize 512x512+0+0 \
				-extent 512x512 $archivo


			else

				continue

			fi
		done
	else


		#manda el tipo de archivo a la variable tipo_archivo

		tipo_archivo=$(file -b "$archivos")

		#chequea que el tipo de archivo sea una imagen

		if [[ $tipo_archivo == *"image"* ]]; then


			#manda el primer nombre a un archivo txt

			echo $(echo $archivos | cut -d "/" -f 2 ) > primer_nombre.txt 


			#chequea que el nombre sea como se indica en el tp


			if [[ $(grep -Ew "^[[:upper:]][[:lower:]]+" primer_nombre.txt) == "" ]];then
				echo "la imagen no tiene un nombre adecuado"
				continue
			fi


			#reescala la imagen a 512x512 y la centra

			convert $archivos -gravity center -resize 512x512+0+0 \
			-extent 512x512 $archivos


		else
			
			continue

		fi
	fi

done

source ./menu.sh
