#!/bin/bash

#limpia los archivos.


 > a.txt
 > todos_nombres.txt

#Verifica que se le hallan asignado archivos.

if [ $# -eq 0 ]; then

	echo "error, no hay archivos para comprimir"

	source ./menu.sh

fi

#El IFS cambia la manera de contar archivos en el for de (espacio) por un (enter).

IFS=$'\n'

for nombre in $1/*;do

#Toma el segundo valor despues de la (/) y luego toma el primer valor, que seria el primer nombre, antes de la (,).

	nombre_limpio=$(echo $nombre | cut -d "/" -f 2 | cut -d "," -f 1)

	echo $nombre_limpio >> todos_nombres.txt

	#Toma los nombres que terminen con la letra a, y luego los guarda en un archivo.

	if [[ $(echo $nombre_limpio | cut -d " " -f 1 | grep -Eo "a$") == "a"  ]]; then

		echo $nombre_limpio >> a.txt

	fi

done

#Cantidad_a, cuenta cuantos nombres que terminan con a, hay, y los cuenta.

cantidad_a=$(wc -l < a.txt)

#Envia un mensaje de la cantidad de personas que su nombre finaliza en a.

echo -e "personas cuyo nombre finaliza con la letra a: ${cantidad_a}" > cantidad_a.txt

#Comprime un archivo con los nombres de las personas, la cantidad de personas que finalizan con a y las imagenes.
mkdir archivos_descomprimidos 2> /dev/null

rm -f ./archivos_descomprimidos/archivo_final.zip 2> /dev/null

zip ./archivos_descomprimidos/archivo_final.zip todos_nombres.txt cantidad_a.txt $1/*

echo "el archivo fue comprimido de forma exitosa!"

source ./menu.sh
