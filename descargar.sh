#!/bin/bash

imagenes=$1
verificacion=$2
echo $1 > ./imagen.txt
echo $2 > ./verificacion.txt

#comprueba que los argumentos pasados sean 2

if [ $# -ne 2 ]; then
	echo "cantidad de argumentos no valida"
	source ./menu.sh
fi


if [[ $(grep -o "^https" ./imagen.txt) != "https" ]]; then
	echo "la url que introdujo es erronea"
	source ./menu.sh
fi

#verifica que los links pasados como argumento sean url verdaderas

if [[ $(grep -o "^https" ./verificacion.txt) != "https" ]]; then
        echo "la url que introdujo es erronea"
        source ./menu.sh
fi

#descarga los argumentos pasados

wget  "$imagenes" -O "imagen.zip"
wget "$verificacion" -O "verificacion"


imagen_verificacion=$(sha256sum ./imagen.zip | cut -d " " -f 1)

#comprueba que la suma de verificacion del archivo descargado sea la misma que la suma verificacion pasada, en caso de ser igual ejecuta source descomprimir.sh con el argumento bajado

if [[ "$imagen_verificacion" == $(cat ./verificacion) ]]; then

	echo "las sumas de verificacion coinciden"
	source descomprimir.sh ./imagen.zip
	if [ $(echo $?) ];then
		rm -rf imagen.zip
		rm -rf verificacion
	fi

else 
	echo "la imagen no coincide con la suma de verificacion"
	rm -rf imagen.zip
	rm -rf verificacion
	source ./menu.sh

fi





