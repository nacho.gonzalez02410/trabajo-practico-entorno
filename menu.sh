#!/bin/bash
OPCION=0


echo "1 - Generar una imagen"
echo "2 - Descargar un archivo comprimido de imagenes"
echo "3 - Si desea procesar las imagenes anteriormente descargadas"
echo "4 - Si desea terminar el programa"

read -p "Elija una opcion: " OPCION

case $OPCION in
	1)
	 source ./generar.sh
	;;
	2)
	 read -p "Ingrese su url del archivo comprimido de imagenes: " ARCHIVO
	 read -p "Ingrese su url del archivo de la suma de verificicion: " VERIF
	 source ./descargar.sh $ARCHIVO $VERIF
	;;
	3)
	 echo "Sus imagenes van a ser procesadas"
	 source ./procesar-descarga.sh
	;;
	4)
	 echo "Muchas gracias."
	 exit 0
	;;
	*)
	 echo "Opcion incorrecta"
	 source ./menu.sh
	;;
esac


