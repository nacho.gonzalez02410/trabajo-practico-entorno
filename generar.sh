#!/bin/bash

#el input de las cantidad de fotos

read -p "Cuantas imagenes quiere generar " CANT


#Elimino la carpeta imagenes por si ya tiene algun archivo

sudo rm -fr fotos 2> /dev/null


#la cantidad de lineas del archivo

total_nombres=$(cat dict.csv | wc -l)


#Verifica que la cantidad de imagenes no sea 0

if [[ $CANT == 0 ]];then
	echo "Error, la cantidad a generar es 0"
	source ./menu.sh
fi


#Creo fotos de nuevo para tenes una carpeta vacía

mkdir fotos 2> /dev/null


for i in $(seq $CANT);do


	#elegir un número aleatorio entre 1 y teoricamente la cantidad de lineas de dict, pero random funciona hasta 32767

	numero_aleatorio=$((RANDOM % $total_nombres + 1))

	#dentro de sed la función es con -n no omitir la salida, después entre " esta la variable con el número aleatorio seguido de p que hace print en la
        #linea seleccionada es decir el número aleatorio anterior y por ultimo el archivo

	nombre_aleatorio=$(sed -n "${numero_aleatorio}p" dict.csv)

 	#imprimo la variable con el nombre, selecciono la primer letra, la traduzco de miniscula a mayuscula y le pego adelante el nombre sin la primer letra
        #que es gracias al formatting con :1

	nombre_mayus=$(echo -n "$nombre_aleatorio" | head -c 1 | tr '[:lower:]' '[:upper:]';echo -n ${nombre_aleatorio:1})


	#Le saco el número y la coma que sigue el nombre

	nombre_final=$(echo $nombre_mayus | cut -d "," -f 1)


	url="https://thispersondoesnotexist.com/"

	#un sleep de 2 para no saturar la pag

	sleep 2

	#wget url -O file funciona como una redirección de la descarga de la imagen

	wget "$url" -O ./fotos/"$nombre_final"



done

source ./procesar.sh fotos



