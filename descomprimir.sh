#!/bin/bash

if [ -e $1 ];then

	ruta_vieja=$PWD

	mkdir ./archivos_descomprimidos 2> /dev/null

	mv -f $1 ./archivos_descomprimidos

	cd ./archivos_descomprimidos

	unzip -uo $1

	case $(echo $?) in
	 "0")
		echo "Proceso exitoso"
		rm -f $1
		cd $ruta_vieja
		source ./menu.sh
	 ;;
	 "1")
		echo "Archivo descomprimido, pero hubo errores"
		rm -f $1
		cd $ruta_vieja
                source ./menu.sh

	 ;;
	 *)
		echo "El archivo no fue descomprimido, hubo errores"
		cd $ruta_vieja
                source ./menu.sh
	 ;;

	esac
else
	echo "No existe el archivo"
	cd $ruta_vieja
        source ./menu.sh

fi
