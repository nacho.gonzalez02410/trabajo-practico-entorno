#!/bin/bash

#verificar que el programa tenga argumentos

if [ $# -eq 0 ];then
        echo "Error, no hay ninguna imagen para procesar"
        source ./menu.sh
fi


#IFS cambia la manera de contar en el for de un espacio a un enter

IFS=$'\n'


for nombres in $1/*;do

	#manda el tipo de archivo a la variable tipo_archivo

	tipo_archivo=$(file -b "$nombres")

	#chequea que el tipo de archivo sea una imagen

	if [[ $tipo_archivo == *"image"* ]]; then


		#manda el primer nombre a un archivo txt

		echo $(echo $nombres | cut -d "/" -f 2 ) > primer_nombre.txt 


		#chequea que el nombre sea como se indica en el tp


		if [[ $(grep -Ew "^[[:upper:]][[:lower:]]+" primer_nombre.txt) == "" ]];then
			echo "la imagen no tiene un nombre adecuado"
			continue
		fi


		#reescala la imagen a 512x512 y la centra

		convert $nombres -gravity center -resize 512x512+0+0 \
		-extent 512x512 $nombres


	else
		
		continue

	fi
done

echo "Se procesaron todas las imagenes con exito"
source ./comprimir.sh fotos


